minetest.register_craft({
      type = "shapeless",
      output = "default:dirt_with_grass",
      recipe = {
	 "default:dirt",
	 "default:grass_1",
	 "default:grass_1",
	 "default:grass_1",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "default:dirt_with_dry_grass",
      recipe = {
	 "default:dirt",
	 "default:dry_grass_1",
	 "default:dry_grass_1",
	 "default:dry_grass_1",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "default:dirt_with_snow",
      recipe = {
	 "default:dirt",
	 "default:snow",
	 "default:snow",
	 "default:snow",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "default:dirt_with_coniferous_litter",
      recipe = {
	 "default:dirt",
	 "default:pine_needles",
	 "default:pine_needles",
	 "default:pine_needles",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "default:dirt_with_rainforest_litter",
      recipe = {
	 "default:dirt",
	 "default:jungleleaves",
	 "default:jungleleaves",
	 "default:jungleleaves",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "default:dry_dirt_with_dry_grass",
      recipe = {
	 "default:dry_dirt",
	 "default:dry_grass_1",
	 "default:dry_grass_1",
	 "default:dry_grass_1",
      },
})

minetest.register_craft({
      type = "shapeless",
      output = "basic_materials:oil_extract 2",
      recipe = {
	 "flowers:sunflower",
	 "flowers:sunflower",
	 "flowers:sunflower",
	 "flowers:sunflower",
      },
})
